# **Parameter estimation for Tsky Model** #
## **Markov Chain Monte Carlo - Metropolis Hastings** ##

-----

## Index ##

* Quick summary:
EDGES is a project in the look for the 21-cm line. Since 2018 they provided information about a potential detection of such signal, but the models are being revised. In particular, the T sky model is under review.
We have applied **Markov Chain Monte Carlo** in particula Metropolis-Hastings, to estimate the proposed model parameters.


* Version:V 1.0
* [Share this repo](https://bitbucket.org/jcardenas0/edges_mcmc_metropolis/)

## Content ##
### What you find here: ###
### Setup and run ###
### Results ###
![Scheme](MCMC_SkyModel/pair_plot_KDE_Test9.png)



